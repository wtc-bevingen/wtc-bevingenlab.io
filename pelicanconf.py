#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Het bestuur'
SITENAME = u'WTC Bevingen'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Brussels'

DEFAULT_LANG = u'nl'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)
LINKS = ()

# Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#           ('Another social link', '#'),)
SOCIAL = ()

DEFAULT_PAGINATION = 50

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

THEME='pelican-elegant-1.3'


PLUGINS = ['pelican_edit_url',]

EDIT_CONTENT_URL = 'https://gitlab.com/wtc-bevingen/wtc-bevingen.gitlab.io/blob/master/{file_path}'
#https://gitlab.com/wtc-bevingen/wtc-bevingen.gitlab.io/edit/master/content/pages/2010_vogezen.md

STATIC_PATHS = ['pages', 'attachments']
