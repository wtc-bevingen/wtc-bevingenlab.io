Title: Rit 32 -- Alpen 2015 dag 5
Date: 2015-08-06 09:00
Tags: Rit, 2015, Alpen
Category: Rit, Alpen 

## Traject

114 km -- 2320 hm

<div style="background-color:#fff;"> <iframe src="http://www.bikemap.net/nl/route/3215466-alpen-dag-5-2015/widget/?width=425&amp;height=350&amp;unit=metric" width="425" height="350" border="0" frameborder="0" marginheight="0" marginwidth="0" scrolling="no"> </iframe> <div style="margin: -4px 0 0 5px; height: 16px; color: #000; font-family: sans-serif; font-size: 12px;"> Route <a href="http://www.bikemap.net/nl/route/3215466-alpen-dag-5-2015/" style="color:#2a88ac; text-decoration:underline;" target="_blank">3.215.466</a> - powered by <a href="http://www.bikemap.net" style="color:#2a88ac; text-decoration:underline;" target="_blank">www.bikemap.net</a> </div> </div>

### Col de la Bonette

Profiel:

![Col de la bonette](http://www.cyclingcols.com/profiles/BonetteN.gif)

[details](http://www.cyclingcols.com/col/Bonette)

[extra info](http://www.klimtijd.nl/beklimming/col-de-la-bonette-jausiers)

## Fotos

Met dank aan Michel Van Stappen.

<table style="width:194px;"><tr><td align="center" style="height:194px;background:url(https://www.gstatic.com/pwa/s/v/lighthousefe_20150804.00_p1/transparent_album_background.gif) no-repeat left"><a href="https://picasaweb.google.com/115868373390539202987/AlpenDag52015?authuser=0&feat=embedwebsite"><img src="https://lh3.googleusercontent.com/-tThBQ-afVGI/VchyntMMzwE/AAAAAAAACjM/hSBdT_S-fYE/s160-c-Ic42/AlpenDag52015.jpg" width="160" height="160" style="margin:1px 0 0 4px;"></a></td></tr><tr><td style="text-align:center;font-family:arial,sans-serif;font-size:11px"><a href="https://picasaweb.google.com/115868373390539202987/AlpenDag52015?authuser=0&feat=embedwebsite" style="color:#4D4D4D;font-weight:bold;text-decoration:none;">Alpen dag 5 (2015)</a></td></tr></table>
