Title: Rit 30 -- Alpen 2015 dag 3
Date: 2015-08-04 09:00
Tags: Rit, 2015, Alpen
Category: Rit, Alpen 

## Traject

108 km -- 2270 hm

<div style="background-color:#fff;"> <iframe src="http://www.bikemap.net/nl/route/3215463-alpen-dag-3-2015/widget/?width=425&amp;height=350&amp;unit=metric" width="425" height="350" border="0" frameborder="0" marginheight="0" marginwidth="0" scrolling="no"> </iframe> <div style="margin: -4px 0 0 5px; height: 16px; color: #000; font-family: sans-serif; font-size: 12px;"> Route <a href="http://www.bikemap.net/nl/route/3215463-alpen-dag-3-2015/" style="color:#2a88ac; text-decoration:underline;" target="_blank">3.215.463</a> - powered by <a href="http://www.bikemap.net" style="color:#2a88ac; text-decoration:underline;" target="_blank">www.bikemap.net</a> </div> </div>

### Col de la machine 

Profiel:

![Col de la machine](http://www.cyclingcols.com/profiles/MachineN.gif)

[details](http://www.cyclingcols.com/col/Machine)


### Col de l'echarasson

![Col de l'echarasson](http://profils.cols-cyclisme.com/2411.gif =400x)

[details](http://www.cols-cyclisme.com/vercors/france/col-de-l-echarasson-depuis-saint-jean-en-royans-c2411.htm)


### Col de la bataille

![Col de la bataille](http://www.cyclingcols.com/profiles/BatailleE.gif)

[details](http://www.cyclingcols.com/col/Bataille)


### Col des limouches

Profiel:
![Col des limouches -- profiles](http://www.cyclingcols.com/profiles/LimouchesE.gif)

[details](http://www.cyclingcols.com/col/Limouches)


## Fotos

Met dank aan Michel Van Stappen.

<table style="width:194px;"><tr><td align="center" style="height:194px;background:url(https://www.gstatic.com/pwa/s/v/lighthousefe_20150804.00_p1/transparent_album_background.gif) no-repeat left"><a href="https://picasaweb.google.com/115868373390539202987/AlpenDag32015?authuser=0&feat=embedwebsite"><img src="https://lh3.googleusercontent.com/--l1sp0ztV0c/VchvovUj33E/AAAAAAAACjY/Cd-8eCCbNRA/s160-c-Ic42/AlpenDag32015.jpg" width="160" height="160" style="margin:1px 0 0 4px;"></a></td></tr><tr><td style="text-align:center;font-family:arial,sans-serif;font-size:11px"><a href="https://picasaweb.google.com/115868373390539202987/AlpenDag32015?authuser=0&feat=embedwebsite" style="color:#4D4D4D;font-weight:bold;text-decoration:none;">Alpen dag 3 (2015)</a></td></tr></table>
