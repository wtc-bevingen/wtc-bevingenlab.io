Title: Nieuwe club kledij
Date: 2016-07-03 17:00
Tags: 2016
Category: News

Vandaag is de nieuwe club kledij beschikbaar.  

Standaard hebben we 3 stuks:

* korte broek <br/> ![Korte broek](/blog/2016/img/kledij_korte_broek.jpg)
* bloes met korte mouwen <br/> ![Korte mouwen](/blog/2016/img/kledij_korte_mouwen.jpg)
* bloes met lange mouwen <br/> ![Lange mouwen](/blog/2016/img/kledij_lange_mouwen.jpg)
  



