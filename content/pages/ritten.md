Title: Ritten
Date: 2016-07-03
Modified: 2016-07-03
Category: WTC
Tags: WTC
Slug: ritten
Authors: Het Bestuur
Summary: Ritten

<ul id="events-upcoming">
</ul>
<ul id="events-past">
</ul>

<script type="text/javascript">
 var fgc_init = {
  calendarUrl: 'https://www.googleapis.com/calendar/v3/calendars/wtc.bevingen@gmail.com/events?key=AIzaSyAGcuqyt_Lj0Fy78ePGEaYxkdShRJfhLgE',
  past: true,
  upcoming: true,
  sameDayTimes: true,
  pastTopN: 50,
  upcomingTopN: 50,
  itemsTagName: 'li',
  upcomingSelector: '#events-upcoming',
  pastSelector: '#events-past',
  upcomingHeading: '<h2>Volgende ritten</h2>',
  pastHeading: '<h2>Voorbije ritten</h2>',
  format: ['*date*', ' &#8212; ', '*summary*', ' <br/>&nbsp;&nbsp;&nbsp;&nbsp;<small>', '*description*', '</small> aan ', '*location*']
 };
</script>

## Referenties
* [ical](https://calendar.google.com/calendar/ical/wtc.bevingen%40gmail.com/public/basic.ics)
* [html](https://calendar.google.com/calendar/embed?src=wtc.bevingen%40gmail.com&ctz=Europe/Brussels)
