Title: Reglement
Date: 2015-03-01
Modified: 2015-03-01
Category: WTC
Tags: WTC
Slug: reglement
Authors: Het Bestuur
Summary: HUISHOUDELIJK  REGLEMENT

# HUISHOUDELIJK  REGLEMENT

1 : Het lidgeld bedraagt € 50 en wordt verminderd voor gezinnen en kansarmen.

2 : Bij elke aankomst van een rit wordt er € 10 gevraagd, waarvoor men 3 drankbonnen ontvangt ter waarde van 6 €. 1 drankbon is goed voor één consumptie tot € 2,00. Indien er iets gedronken wordt van € 3 , moet er dus  € 1 bijgelegd worden . De rest is voor alg. kosten en voor andere organisaties bv. Clubfeest.

3 : Elke rit vertrekt aan Taverne De Kanarie en telt mee voor de kampioenstitel. Ook bij slechte weersomstandigheden indien de veiligheid om te fietsen niet in gedrang komt en er tenminste 6 vertrekkers zijn.

4 : Het uur van vertrek dient steeds nageleefd te worden zoals vermeld in het clubboekje, tenzij anders meegedeeld door het bestuur.

5 : Er worden 100 punten verdiend bij de start van elke rit , ook voor de bestuurder van de volgwagen  , alsook voor de leden die komen helpen bij een organisatie van de club .,dwz zowel op de dag van de voorbereiding 100 punten als de dag van de organisatie zelf 100 punten .

6 : Voor de ritten in clubverband verdient men 1 punt per gereden km (ook bij pech ) , behalve als er een meerdaagse trip wordt georganiseerd . Enkel bij een gelijke stand na de laatste rit krijgt de persoon die actief deelgenomen heeft 1 punt extra . 

7 : Elke rit is vatbaar voor wijzigingen door toedoen van de weersomstandigheden, mechanische pech, valpartijen , geen volgwagen of op beslissing van het bestuur .

8 : De gemiddelde snelheid per rit is vastgelegd tussen  26  en 27 km per uur en in groep wordt er met maximum 2 naast mekaar gereden.

9 : Het is toegestaan om af en toe wat harder te fietsen nl. op de hellingen en op de parcours  Ulbeek -- Zepperen, Buvingen -- Kerkom, Heide -- Melveren en Hélécine – Orsmaal. 

**TIJDENS DE REST VAN DE RIT WORDT ER IN GROEP GEREDEN.**  
**DE 100 PUNTEN VAN DE START WORDEN NIET TOEGEKEND AAN PERSONEN DIE ZICH DAAR NIET AAN HOUDEN !**

10 : Er wordt steeds gewacht bovenaan een berg of aan een kruispunt tot de laatste aansluit.

11 : Bekeuringen uitgeschreven door het niet naleven van de verkeersreglementen zijn ten laste van de betrokken persoon.

12 : Bij inschrijving van een feest of van een geplande activiteit dient elk lid de organisator 
STEEDS  te verwittigen EN wordt er gevraagd om een handje te komen helpen .

13 : Het is verplicht om de clubkledij te dragen tijdens de ritten in clubverband , behalve voor nieuwe leden en  indien er 2 dagen na elkaar een rit gepland is .

14 : Ongepast gedrag zoals pesten, racisme, drugsgebruik, motortjes ea. wordt niet getolereerd. 


De statuten van de sportvereniging als ook het huishoudelijk reglement zijn ter inzage bij Penningmeester Vanbrabant Guido .
