Title: Wieler Touristen Club Bevingen
URL:
save_as: index.html
status: hidden

<ul id="events-upcoming">
</ul>

## Kampioenen

![]({attach}images/2016_kampioenen.jpg)

## Fiets vakanties

- [Vogezen]({filename}2010_vogezen.md)
- [Alpen]({filename}2015_alpen.md)
<iframe width="560" height="315" src="https://www.youtube.com/embed/48FuomAjVYs?rel=0" frameborder="0" allowfullscreen></iframe>


## Lid worden?

Heb je de smaak te pakken, maar lid worden?  
Kom gerust vrijblijvend eens een rit met ons meerijden!  
We vertrekken aan de cafe Kanaries in Sint-Truiden, het vertrekuur vind je hierboven bij 'Volgende ritten'.


Bedankt voor je bezoek! 


<script type="text/javascript">
 var fgc_init = {
  calendarUrl: 'https://www.googleapis.com/calendar/v3/calendars/wtc.bevingen@gmail.com/events?key=AIzaSyAGcuqyt_Lj0Fy78ePGEaYxkdShRJfhLgE',
  past: true,
  upcoming: true,
  sameDayTimes: true,
  pastTopN: 0,
  upcomingTopN: 3,
  itemsTagName: 'li',
  upcomingSelector: '#events-upcoming',
  pastSelector: '#events-past',
  upcomingHeading: '<h2>Volgende ritten</h2>',
  pastHeading: '<h2>Voorbije ritten</h2>',
  format: ['*date*', ' &#8212; ', '*summary*', ' <br/>&nbsp;&nbsp;&nbsp;&nbsp;<small>', '*description*', '</small> aan ', '*location*']
 };
</script>
